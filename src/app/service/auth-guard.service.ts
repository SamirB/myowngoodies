import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable()
export class AuthGuardService implements CanActivate {

  constructor(private authService: AuthService,
    private router: Router) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    this.authService.authStatutSubject.subscribe(

      (value) => {
        if (value)
          return true;
        else
          this.router.navigate(['/login']);
      },
      () =>{
        return false;
      } 
    );
    if(!this.authService.isAuth)
      this.router.navigate(['/login']);
    return this.authService.isAuth;

  }
}
