import { Injectable } from '@angular/core';
import {Product} from '../model/product';
import {Observable, Subject} from 'rxjs';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class ProductService {

  lstProducts: Product[];
  productSubject = new Subject<Product[]>();

  constructor(private httpClient: HttpClient) {
  }


  emitProducts() {
    this.productSubject.next(this.lstProducts.slice());
  }

  addProduct(product: Product) {

    this.lstProducts.push(product);
    this.emitProducts();

    this.httpClient
      .post<Product>('http://localhost:9090/Products', product).subscribe(
      () => {
        console.log('It s good !' );
      },
      (error) => {
        console.log('Erreur ! : ' + error);
      }
    );


  }

  getProductsFromServer() {
    this.httpClient
      .get<any[]>('http://localhost:9090/Products')
      .subscribe(
        (response) => {
          this.lstProducts = response;
          this.emitProducts();
        },
        (error) => {
          console.log('Erreur ! : ' + error);
        }
      );
  }

  getProductByReference(reference: string): Observable<Product> {
    return this.httpClient
      .get<Product>('http://localhost:9090/Products/' + reference);
  }

  getProductsByCategory(category: string): Observable<Product[]> {
    return this.httpClient
      .get<Product[]>('http://localhost:9090/Products/Category/' + category);
  }

  getAllCategory(): Observable<Product[]> {
    return this.httpClient
      .get<Product[]>('http://localhost:9090/Products/Category/');
  }

  getProductsBySearch(search: string): Observable<Product[]> {
    return this.httpClient
      .get<Product[]>('http://localhost:9090//Products/Search/' + search);
  }

}

