import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DiscountCode } from '../model/discount-code';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DiscountCodeService {

  constructor(private httpClient: HttpClient) { }


  addDiscountCode(discountCode: DiscountCode) {

    this.httpClient
      .post<DiscountCode>('http://localhost:9090/DiscountCode', discountCode).subscribe(
        () => {
          console.log('It s good !');
        },
        (error) => {
          console.log('Erreur ! : ' + error);
        }
      );
  }

  getDiscountCodeByCode(code: string): Observable<DiscountCode> {
    return this.httpClient
      .get<DiscountCode>('http://localhost:9090/DiscountCode/' + code);
  }
}
