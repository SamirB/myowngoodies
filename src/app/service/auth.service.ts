import { Injectable } from '@angular/core';
import { User } from '../model/user';
import { UserService } from './user.service';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {


  isAuth = false;
  userAuth: User;
  userSubject = new Subject<User>();
  authStatutSubject = new Subject<boolean>();

  constructor(private userService: UserService) { }

  emitUser() {
    this.userSubject.next(this.userAuth);
    this.emitAuthStatut();
  }

  emitAuthStatut() {
    this.authStatutSubject.next(this.isAuth);
  }

  signIn(user: User) {

    this.isAuth = false;

    this.userService.getUserByPasswordAndMailOrUsername(user).subscribe(

      (value) => {
        this.userAuth = value;

        if (this.userAuth != null)
          this.isAuth = true;

        this.emitUser();
      },
      (error) => {
        console.log('Error' + error);
      }
    );
  }

  signOut() {
    this.isAuth = false;
    this.userAuth = null;
    this.emitUser();
  }
}
