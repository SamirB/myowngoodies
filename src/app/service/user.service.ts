import { Injectable } from '@angular/core';
import { User } from '../model/user';
import { HttpClient } from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpClient: HttpClient) { }


  addUser(user: User) {

    this.httpClient
      .post<User>('http://localhost:9090/User', user).subscribe(
        () => {
          console.log('User created.');
        },
        (error) => {
          console.log('Erreur ! : ' + error);
        }
      );
  }
  
  getUserByPasswordAndMailOrUsername(user: User): Observable<User> {
    return this.httpClient
    .post<User>('http://localhost:9090/User/Auth', user);
  }
}
