import {Cart} from '../model/cart';
import {Injectable} from '@angular/core';
import {ProductService} from './product.service';
import {HttpClient} from '@angular/common/http';
import {Observable, Subject, Subscriber} from 'rxjs';
import {Product} from '../model/product';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  cart: Cart = new Cart([], 0 , 0 ,null);
  cartSubject = new Subject<Cart>();

  constructor(private productService: ProductService, private httpClient: HttpClient) {
    this.emitAppareilSubject();
  }

  emitAppareilSubject(): void {
    this.cartSubject.next(this.cart);
  }

  mergeCart() {
    this.httpClient
      .put<Cart>('http://localhost:9090/Cart/', this.cart).subscribe(
      (response) => {
        this.cart = response;
        this.emitAppareilSubject();
      },
      (error) => {
        console.log('Erreur ! : ' + error);
      }
    );
  }

  addProductToCart(reference: string) {
    this.productService.getProductByReference(reference).subscribe(
      (response) => {
        this.cart.lstProduct.push(response);
        this.mergeCart();
      },
      (error) => {
        console.log('Erreur ! : ' + error);
      }
    );
  }

  removeProductToCart(product: Product) {

    const index = this.cart.lstProduct.indexOf(product);
    this.cart.lstProduct.splice(index, 1);
    this.mergeCart();

  }



}
