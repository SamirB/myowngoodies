import {Component, Input, OnInit} from '@angular/core';
import {CartService} from '../../service/cart-service';

@Component({
  selector: 'app-add-to-cart-button',
  templateUrl: './add-to-cart-button.component.html',
  styleUrls: ['./add-to-cart-button.component.scss']
})
export class AddToCartButtonComponent implements OnInit {

  @Input() reference: string;

  constructor(private cartservice: CartService) { }

  ngOnInit() {
  }

  addToCart() {
    this.cartservice.addProductToCart(this.reference);
  }
}
