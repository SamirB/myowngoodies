import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';


// Service import
import {AuthService} from './service/auth.service';
import {ProductService} from './service/product.service';
import {CartService} from './service/cart-service';
import {DiscountCodeService} from './service/discount-code.service';
import { UserService } from './service/user.service';

// Component import
import { AppComponent } from './app.component';
import { NavbarGlobalComponent } from './component/navbar-global/navbar-global.component';
import { LoginComponent } from './component/login/login.component';
import { HomeComponent } from './component/home/home.component';
import { ProductComponent } from './component/product/product.component';
import { NewProductComponent } from './component/new-product/new-product.component';
import { ProductDetailComponent } from './component/product-detail/product-detail.component';
import { AddToCartButtonComponent } from './button/add-to-cart-button/add-to-cart-button.component';
import { CartComponent } from './component/cart/cart.component';
import { ConfirmModalComponent } from './modal/confirm-modal/confirm-modal.component';
import { SearchComponent } from './component/search/search.component';
import { NewDiscountCodeComponent } from './component/new-discount-code/new-discount-code.component';
import { NewUserComponent } from './component/new-user/new-user.component';
import { UserComponent } from './component/user/user.component';
import { AuthGuardService } from './service/auth-guard.service';


const appRoutes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'home', component: HomeComponent },
  { path: 'home/:category', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'newproduct', component: NewProductComponent },
  { path: 'newDiscountCode', component: NewDiscountCodeComponent },
  { path: 'newUser', component: NewUserComponent },
  { path: 'product-detail/:reference', component: ProductDetailComponent },
  { path: 'cart', component: CartComponent },
  { path: 'search/:search', component: SearchComponent },
  // { path: 'user', component: UserComponent },
  { path: 'user',canActivate: [AuthGuardService], component: UserComponent },

];

@NgModule({
  declarations: [
    AppComponent,
    NavbarGlobalComponent,
    LoginComponent,
    HomeComponent,
    ProductComponent,
    NewProductComponent,
    ProductDetailComponent,
    AddToCartButtonComponent,
    CartComponent,
    ConfirmModalComponent,
    SearchComponent,
    NewDiscountCodeComponent,
    NewUserComponent,
    UserComponent
  ],
  imports: [
    BrowserModule,
    // Rooting access
    AppRoutingModule,
    RouterModule.forRoot(appRoutes),
    // Reactive form access
    FormsModule,
    ReactiveFormsModule,
    // HttpClient access
    HttpClientModule,
  ],
  providers: [
    AuthService,
    AuthGuardService,
    UserService,
    ProductService,
    CartService, 
    DiscountCodeService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
