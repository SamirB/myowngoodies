export class Discount {

    constructor(
        public startDate: Date,
        public endDate: Date,
        public price: number,
        public active: boolean
      ) {}
}
