import { Discount } from './discount';

export class Product {

  constructor(
    public id: number,
    public name: string,
    public reference: string,
    public description: string,
    public price: number,
    public category: string,
    public discount: Discount,
  ) {}

}

