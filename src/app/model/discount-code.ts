export class DiscountCode {


    constructor(
        public code: string,
        public startDate: Date,
        public endDate: Date,
        public price: number,
        public active: boolean
    ){}

}
