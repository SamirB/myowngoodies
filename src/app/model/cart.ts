import {Product} from './product';
import { DiscountCode } from './discount-code';

export class Cart {

  constructor(
    public lstProduct: Product[],
    public price: number,
    public totalPrice: number,
    public discountCode: DiscountCode 
  ) {}

}
