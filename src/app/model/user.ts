import { Adress } from './adress';

export class User {

    constructor(
        public firstName: string,
        public lastName: string,
        public username: string,
        public phoneNumber: string,
        public mail: string,
        public password: string,  

        public credits: number,
        public bornDate: Date,
        public adress: Adress
    ){}

    
}
