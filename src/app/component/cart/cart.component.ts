import { Component, OnInit } from '@angular/core';
import { CartService } from '../../service/cart-service';
import { Cart } from '../../model/cart';
import { Product } from '../../model/product';
import { NgForm } from '@angular/forms';
import { DiscountCode } from 'src/app/model/discount-code';
import { DiscountCodeService } from 'src/app/service/discount-code.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
  cart: Cart;
  alert: boolean;

  constructor(private cartService: CartService,
    private discountCodeService: DiscountCodeService) {
    this.cart = this.cartService.cart;
  }

  ngOnInit() {
    this.cartService.cartSubject.subscribe(value => {
      this.cart = value;
    });
  }

  removeToCart(product: Product) {
    this.cartService.removeProductToCart(product);
  }
  buy() {
    this.alert = true;
  }

  findDiscountCode(form: NgForm) {
 
    this.discountCodeService.getDiscountCodeByCode(form.value.discountCode).subscribe(
      (response) => {
        if (response == null)
          console.log('This discount code dont exist.');
        else if (!response.active)
          console.log('This discount code is not active.');
        else{
          this.cart.discountCode = response;
          this.cartService.cartSubject.next(this.cart);
          this.cartService.mergeCart();
        }
      },
      (error) => {
        console.log('Error' + error);
      }
    );
  }
}
