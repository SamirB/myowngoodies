import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { DiscountCodeService } from 'src/app/service/discount-code.service';
import { DiscountCode } from 'src/app/model/discount-code';
import { Adress } from 'src/app/model/adress';
import { User } from 'src/app/model/user';
import { UserService } from 'src/app/service/user.service';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.scss']
})
export class NewUserComponent implements OnInit {


  productForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
    private userService: UserService) { }

  ngOnInit() {
    this.initForm();
  }


  initForm() {
    this.productForm = this.formBuilder.group({
      firstName: '',
      lastName: '',
      username: '',
      phoneNumber: '',
      mail: '',
      password: '',
      bornDate: '',
      adress: this.formBuilder.group({
        street: '',
        city: '',
        state: '',
      })
    });
  }

  onSubmitForm() {
    const formValue = this.productForm.value;

    const newAdress = new Adress(
      formValue.adress.street,
      formValue.adress.city,
      formValue.adress.state
    );


    const newUser = new User (
      formValue.firstName,
      formValue.lastName,
      formValue.username,
      formValue.phoneNumber,
      formValue.mail,
      formValue.password,
      0,
      formValue.bornDate,
      newAdress
    );
    this.userService.addUser(newUser);
    console.log(newUser);
    this.productForm.reset();
  }
}
