import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ProductService} from '../../service/product.service';
import {Product} from '../../model/product';
import { Discount } from 'src/app/model/discount';

@Component({
  selector: 'app-new-product',
  templateUrl: './new-product.component.html',
  styleUrls: ['./new-product.component.scss']
})
export class NewProductComponent implements OnInit {

  productForm: FormGroup;
  lstCategory: any[];

  constructor(private formBuilder: FormBuilder,
              private productService: ProductService) {
  }

  ngOnInit() {
    this.initForm();
    this.productService.getAllCategory().subscribe(value => {
        this.lstCategory = value;
      }
    );
  }

  initForm() {
    this.productForm = this.formBuilder.group({
      name: '',
      reference: '',
      description: '',
      price: 0,
      category: 'OTHER',

      discount: this.formBuilder.group({
        price: 0,
       startDate: '',
        endDate: ''
       })
      
    });
  }

  onSubmitForm() {
    const formValue = this.productForm.value;

    const newDiscount = new Discount(
      formValue.discount.startDate,
      formValue.discount.endDate,
      formValue.discount.price,
      false
    );


    const newProduct = new Product(
      0,
      formValue.name,
      formValue.reference,
      formValue.description,
      formValue.price,
      formValue.category,
      newDiscount
    );
    this.productService.addProduct(newProduct);
    this.productForm.reset();
  }

}
