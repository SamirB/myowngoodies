import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import { Discount } from 'src/app/model/discount';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {


  @Input() name: string;
  @Input() reference: string;
  @Input() price: number;
  @Input() discount: Discount;


  constructor(private router: Router) {
  }

  ngOnInit() {
  }

  goToProduct() {
    this.router.navigate(['product-detail/' + this.reference]);
  }

}
