import {Component, OnInit} from '@angular/core';
import {ProductService} from '../../service/product.service';
import {Product} from '../../model/product';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})

export class HomeComponent implements OnInit {


  public lstProducts: Product[];

  constructor(private productService: ProductService, private route: ActivatedRoute) {
  }

  ngOnInit() {
    const category = this.route.snapshot.params.category;

    if (category !== undefined) {
      this.route.params.subscribe(params => this.getProductsByCategory(params.category));
    } else {
      this.getProducts();
    }

  }

  private getProductsByCategory(category: string) {
    this.productService.getProductsByCategory(category).subscribe(
      (response) => {
        this.lstProducts = response;
      },
      (error) => {
        console.log('Erreur ! : ' + error);
      }
    );
  }

  private getProducts() {
    this.productService.productSubject.subscribe(
      (response) => {
        this.lstProducts = response;
      },
      (error) => {
        console.log('Erreur ! : ' + error);
      }
    );
    this.productService.getProductsFromServer();
  }


}
