import {Component, OnInit} from '@angular/core';
import {CartService} from '../../service/cart-service';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup} from '@angular/forms';
import { AuthService } from 'src/app/service/auth.service';

@Component({
  selector: 'app-navbar-global',
  templateUrl: './navbar-global.component.html',
  styleUrls: ['./navbar-global.component.scss']
})
export class NavbarGlobalComponent implements OnInit {

  cartCounter = 0;
  searchForm: FormGroup;
  authStatus: boolean;

  constructor(private cartService: CartService, private router: Router, 
    private formBuilder: FormBuilder, private authService: AuthService) {
  }

  ngOnInit() {
    this.initForm();
    this.cartService.cartSubject.subscribe(value => {
      this.cartCounter = value.lstProduct.length;
    });
    this.authStatus = this.authService.isAuth ;
  }


  initForm() {
    this.searchForm = this.formBuilder.group({
      searchArg: ''
    });
  }

  search() {
    if (this.searchForm.value.searchArg === '') {
      this.searchForm.value.searchArg = 'ref';
    }
    this.router.navigate(['search/' + this.searchForm.value.searchArg]);
  }

}
