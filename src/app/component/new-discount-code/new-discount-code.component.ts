import { Component, OnInit } from '@angular/core';
import { DiscountCode } from 'src/app/model/discount-code';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DiscountCodeService } from 'src/app/service/discount-code.service';

@Component({
  selector: 'app-new-discount-code',
  templateUrl: './new-discount-code.component.html',
  styleUrls: ['./new-discount-code.component.scss']
})
export class NewDiscountCodeComponent implements OnInit {

  productForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
    private discountCodeService: DiscountCodeService) { }

  ngOnInit() {
    this.initForm();
  }


  initForm() {
    this.productForm = this.formBuilder.group({
      code: '',
      startDate: '',
      endDate: '',
      price: 0
    });
  }

  onSubmitForm() {
    const formValue = this.productForm.value;

    const newDiscountCode = new DiscountCode(
      formValue.code,
      formValue.startDate,
      formValue.endDate,
      formValue.price,
      false
    );

    this.discountCodeService.addDiscountCode(newDiscountCode);
    this.productForm.reset();
  }


}
