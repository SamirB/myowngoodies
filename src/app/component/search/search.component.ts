import {Component, OnInit} from '@angular/core';
import {Product} from '../../model/product';
import {ProductService} from '../../service/product.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  public lstProducts: Product[];

  constructor(private productService: ProductService, private route: ActivatedRoute) {
  }

  ngOnInit() {
    const category = this.route.snapshot.params.searcharg;

    this.route.params.subscribe(params => this.search(params.search));
  }

  private search(searchArg: string) {
    this.productService.getProductsBySearch(searchArg).subscribe(
      (response) => {
        this.lstProducts = response;
      },
      (error) => {
        console.log('Erreur ! : ' + error);
      }
    );
  }

}
