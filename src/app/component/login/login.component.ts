import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/service/auth.service';
import { User } from 'src/app/model/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {


  signupForm: FormGroup;
  errorMessage: string;

  constructor(private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router) {
  }

  ngOnInit() {

    if (this.authService.isAuth)
      this.router.navigate(['user']);

    this.authService.authStatutSubject.subscribe(
      (value) => {
        if (value == true)
          this.router.navigate(['user']);
      }
    );
    this.authService.userSubject.subscribe(
      (value) => {
        if (value == null || value == undefined)
          this.signupForm.reset();
      }
    )
    this.initForm();
  }


  initForm() {
    this.signupForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.pattern(/[0-9a-zA-Z]{6,}/)]]
    });
  }

  onSignIn() {
    const formValue = this.signupForm.value;

    let user = new User('', '',
      formValue.email,
      '',
      formValue.email,
      formValue.password,
      0, null, null);

    this.authService.signIn(user);
  }

  onSignOut() {
    this.authService.signOut();
  }

}
