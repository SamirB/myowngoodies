import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/service/user.service';
import { User } from 'src/app/model/user';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  authUser: User;

  constructor(private authService: AuthService,
    private router: Router) { }

  ngOnInit() {
    this.authUser = this.authService.userAuth;

    this.authService.userSubject.subscribe(
      (value) => {
        this.authUser = value;
        if(this.authUser == null || undefined)
        this.router.navigate(['login']);
      }
    );
  }
  onSignOut() {
    this.authService.signOut();
  }

}
