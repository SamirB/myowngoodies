import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Product} from '../../model/product';
import {ProductService} from '../../service/product.service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit {
  currentProduct: Product;

  constructor(private route: ActivatedRoute, private productService: ProductService) {
  }

  ngOnInit() {
    this.productInit();
    const reference = this.route.snapshot.params.reference;

    this.productService.getProductByReference(reference).subscribe(
      (response) => {
        this.currentProduct = response;
      },
      (error) => {
        console.log('Erreur ! : ' + error);
      }
    );
  }

  productInit() {
    this.currentProduct = new Product(-1, 'Init', 'ref00000', 'Init', 0, 'OTHER', null);
  }

}
